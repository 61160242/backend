const User = require('../models/User')
const userController = {
  userList: [
    { id: 1, name: 'Kantapond', gender: 'F' },
    { id: 2, name: 'prasob', gender: 'M' }
  ],
  lastId: 3,
  async addUser(req, res) {
    const payload = req.body
    console.log(payload)
    const user = new User(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser(req, res) {
    const payload = req.body
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)

    }
  },
  async deleteUser(req, res) {
    const { id } = req.params

    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers(req, res) {

    try {
      const user = await User.find({})
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser(req, res) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }



  }

}
module.exports = userController
